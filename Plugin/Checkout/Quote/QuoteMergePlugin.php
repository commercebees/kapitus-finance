<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Plugin\Checkout\Quote;

use Kapitus\Finance\Helper\Data;
use Magento\Framework\Message\ManagerInterface;
use Magento\Quote\Model\Quote;

/**
 * Class QuoteMergePlugin
 *
 * @package Kapitus\Finance\Plugin\Checkout\Quote
 */
class QuoteMergePlugin
{
    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var
     */
    protected $customerQuote;


    protected $quoteRepository;

    /**
     * QuoteMergePlugin constructor.
     *
     * @param ManagerInterface $messageManager
     * @param Data             $helperData
     */
    public function __construct(
        ManagerInterface $messageManager,
        Data $helperData,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
    ) {
        $this->helperData = $helperData;
        $this->messageManager = $messageManager;
    $this->quoteRepository = $quoteRepository;
    }

    /**
     * @param \Magento\Checkout\Model\Session $subject
     * @param                                 $result
     *
     * @return mixed
     */
    public function afterLoadCustomerQuote(\Magento\Checkout\Model\Session $subject, $result)
    {
        $this->customerQuote = $result->getQuote();
        if ($this->helperData->getScopeConfig('active') == 1) {
            $this->checkQuoteIsFinanceAndRemove($this->customerQuote, $subject);
        $subject->getQuote()->setData('trigger_recollect', 1)
        ->setTotalsCollectedFlag(false)
        ->collectTotals()->save();
        }
    
    
        return $result;
    }

    /**
     * @param $quote
     *
     * @return $this
     */
    protected function checkQuoteIsFinanceAndRemove($quote)
    {
        $name = array();
        $hasFinance = array();
        foreach ($quote->getAllVisibleItems() as $item) {
            if (!$item->getProduct()->getHasFinance()) {
                $name[$item->getId()] = $item->getProduct()->getName();
            } else {
                $hasFinance[] = $item->getId();
            }
        }

        if (count($hasFinance) > 0 && count($name) > 0) {
            foreach ($name as $id => $itemName) {
                $quote->removeItem($id)->setTotalsCollectedFlag(false)->collectTotals()->save();
            }
    
        $quote->setTotalsCollectedFlag(false)
            ->collectTotals();
        $this->quoteRepository->save($quote);

            $this->messageManager->addErrorMessage(
                __(
                    'Mixed cart is not allowed, removed products from cart plugin - %1.',
                    join(' , ', $name)
                )
            );
        }

    $quote->setTotalsCollectedFlag(false)
            ->collectTotals();
    $this->quoteRepository->save($quote);

        return $this;
    }

}
