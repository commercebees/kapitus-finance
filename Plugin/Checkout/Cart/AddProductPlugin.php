<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Plugin\Checkout\Cart;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class AddProductPlugin
 *
 * @package Kapitus\Finance\Plugin\Checkout\Cart
 */
class AddProductPlugin
{

    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Kapitus\Finance\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Kapitus\Finance\Helper\ProductHelper
     */
    protected $helperProduct;

    /**
     * AddProductPlugin constructor.
     *
     * @param \Magento\Checkout\Model\Session          $checkoutSession
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Kapitus\Finance\Helper\Data             $helperData
     * @param \Kapitus\Finance\Helper\ProductHelper    $productHelper
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Kapitus\Finance\Helper\Data $helperData,
        \Kapitus\Finance\Helper\ProductHelper $helperProduct
    ) {
        $this->helperData = $helperData;
        $this->quote = $checkoutSession->getQuote();
        $this->productRepository = $productRepository;
        $this->helperProduct = $helperProduct;
    }

    /**
     * @param      $subject
     * @param      $productInfo
     * @param null $requestInfo
     *
     * @return array
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function beforeAddProduct($subject, $productInfo, $requestInfo = null)
    {
        if ($this->helperData->getScopeConfig('active') == 1) {
            if ($requestInfo instanceof \Magento\Framework\DataObject) {
                $requestObject = $requestInfo;
            } else {
                $requestObject = new \Magento\Framework\DataObject($requestInfo);
            }

            $product = $this->getProductById($requestObject->getProduct());

            if (!(int) $product->getHasFinance() && (int) $this->helperData->getScopeConfig('allow_nonfinancial') === 1) {
                $this->getCheckAllowProduct();
            }

            if ($requestObject->hasData('mixed_card')) {
                $this->checkCartItems($product->getHasFinance(), true);
            } else {
                if (!$this->checkCartItems($product->getHasFinance(), false)) {
                    throw new LocalizedException($this->helperData->getNonDisallowMessage());
                }
            }
        }

        return array($productInfo, $requestInfo);
    }

    /**
     * @param      $hasFinance
     * @param bool $remove
     *
     * @return bool
     */
    public function checkCartItems($hasFinance, $remove = false)
    {
        foreach ($this->quote->getAllVisibleItems() as $item) {
            if ($item->getProduct()->getHasFinance() != $hasFinance) {
                if ($remove) {
                    $this->quote->removeItem($item->getItemId());
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function getCheckAllowProduct()
    {
        if ($this->helperProduct->isFinanceCart()) {
            throw new LocalizedException($this->helperData->getDisallowMessage());
        }

        return true;
    }

    /**
     * @param $id
     *
     * @return \Magento\Catalog\Api\Data\ProductInterface|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductById($id)
    {
        return $this->productRepository->getById($id);
    }
}
