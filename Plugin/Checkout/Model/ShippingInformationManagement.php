<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Plugin\Checkout\Model;

/**
 * Class ShippingInformationManagement
 *
 * @package Kapitus\Finance\Plugin\Checkout\Model
 */
class ShippingInformationManagement
{

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Kapitus\Finance\Helper\Data
     */
    protected $_helper;

    /**
     * ShippingInformationManagement constructor.
     *
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Checkout\Model\Session            $checkoutSession
     * @param \Kapitus\Finance\Helper\Data               $helper
     */
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Kapitus\Finance\Helper\Data $helper
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->_checkoutSession = $checkoutSession;
        $this->_helper = $helper;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $shipping
     * @param                                                       $result
     *
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $shipping,
        $result
    ) {
        $cartId = $this->_checkoutSession->getQuote()->getId();
        $quote = $this->quoteRepository->getActive($cartId);
        if ($quote->getSubmissionId() != '' && $this->_helper->getScopeConfig('active') == 1) {
            $this->_helper->updateSubmission($cartId, $quote->getSubmissionId());
            return $result;
        } else {
            return $result;
        }
    }
}
