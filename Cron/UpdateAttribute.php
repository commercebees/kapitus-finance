<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Cron;

/**
 * Class UpdateAttribute
 *
 * @package Kapitus\Finance\Cron
 */
class UpdateAttribute
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Action
     */
    protected $action;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Kapitus\Finance\Helper\Data
     */
    protected $helper;

    /**
     * VersionChecker constructor.
     *
     * @param \Psr\Log\LoggerInterface                                     $logger
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Product\Action                  $action
     * @param \Magento\Store\Model\StoreManagerInterface                     $storeManager
     * @param \Kapitus\Finance\Helper\Data                        $helper
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Action $action,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Kapitus\Finance\Helper\Data $helper
    
    ) {
        $this->logger = $logger;
        $this->productCollectionFactory = $productCollectionFactory;
    $this->action = $action;
    $this->_storeManager = $storeManager;
    $this->helper = $helper;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
    $attribute_value = $this->helper->getScopeConfig('update_attribute');
    if($attribute_value) {
    $productIds = $this->getProductIdCollection();
    $store_id = $this->_storeManager->getStore()->getId();
        try {
            $this->action->updateAttributes($productIds, array('has_finance' => 1), $store_id);
        } catch (\Exception $e) {
            $this->logger->addInfo("Update Finace with kapitus attribute Error" . $e);
        }
    }
    }
    
    /**
     * @return array
     */
    public function getProductIdCollection()
    {

        $collection = $this->productCollectionFactory->create();
    $collection->addFieldToFilter('has_finance', array('eq' => 0));
        
    $array_product = array();
    foreach ($collection as $product) {
        $array_product[] = $product->getId();
    }

    return $array_product;
    }
}
