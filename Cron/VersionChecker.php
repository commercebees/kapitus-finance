<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Cron;

/**
 * Class VersionChecker
 *
 * @package Kapitus\Finance\Cron
 */
class VersionChecker
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Notification\MessageInterface
     */
    protected $versionNotifier;

    /**
     * VersionChecker constructor.
     *
     * @param \Psr\Log\LoggerInterface                         $logger
     * @param \Magento\Framework\Notification\MessageInterface $versionNotifier
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Notification\MessageInterface $versionNotifier
    ) {
        $this->logger = $logger;
        $this->versionNotifier = $versionNotifier;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        try {
            $this->versionNotifier->UpdateAvailable();
        } catch (\Exception $e) {
            $this->logger->addInfo("creon kapitus version update Error" . $e);
        }
    }
}