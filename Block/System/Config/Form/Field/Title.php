<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Block\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Title
 *
 * @package Kapitus\Finance\Block\System\Config\Form\Field
 */
class Title extends \Magento\Config\Block\System\Config\Form\Field
{

  protected function _getElementHtml(AbstractElement $element) 
  {
        $element->setValue('Kapitus Financing');        
        $element->setDisabled('disabled');
        return $element->getElementHtml();

  }
    
}
