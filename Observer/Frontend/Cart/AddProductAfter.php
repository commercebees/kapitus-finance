<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Observer\Frontend\Cart;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class AddProductAfter
 *
 * @package Kapitus\Finance\Observer\Frontend\Cart
 */
class AddProductAfter implements ObserverInterface
{

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Kapitus\Finance\Helper\Data
     */
    protected $helperdata;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * AddProductAfter constructor.
     *
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Checkout\Model\Session     $checkoutSession
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Kapitus\Finance\Helper\Data $helperdata,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->request = $request;
        $this->helperdata = $helperdata;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->helperdata->getScopeConfig('active') == 1) {
            $isFinance = $this->request->getPost('finance');
            if ($isFinance) {
                $this->checkoutSession->setFinMethod(true);
            }else{
                $this->checkoutSession->setFinMethod(false);
            }

            $isPopup = $this->request->getPost('popup');
            if ($isPopup == 1) {
                $this->checkoutSession->setShowpopup(true);
            }
        }

    }
}