<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Observer\Frontend\Sales;

/**
 * Class QuoteCollectTotalsAfter
 *
 * @package Kapitus\Finance\Observer\Frontend\Sales
 */
class QuoteCollectTotalsAfter implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Kapitus\Finance\Helper\Data
     */
    protected $helperdata;

    /**
     * QuoteCollectTotalsAfter constructor.
     *
     * @param \Kapitus\Finance\Helper\Data $helperdata
     */
    public function __construct(
        \Kapitus\Finance\Helper\Data $helperdata
    ) {
        $this->helperdata = $helperdata;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        if ($this->helperdata->getScopeConfig('active') == 1) {
            $quote = $observer->getEvent()->getQuote();
            if ($quote->getSubmissionId()) {
               // $this->helperdata->updateSubmission($quote->getId(), $quote->getSubmissionId());
            }
        }
    }

}
