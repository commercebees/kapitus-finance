<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Observer\Frontend;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class PaymentMethodAvailable
 *
 * @package Kapitus\Finance\Observer\Frontend
 */
class PaymentMethodAvailable implements ObserverInterface
{

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     * @var \Kapitus\Finance\Helper\Data
     */
    protected $helperdata;

    /**
     * PaymentMethodAvailable constructor.
     *
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Kapitus\Finance\Helper\Data    $helperdata
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Kapitus\Finance\Helper\Data $helperdata
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->helperdata = $helperdata;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->helperdata->getScopeConfig('active') == 1) {
            if ($this->checkNonFinance()) {
                // Cart Item there is no Finance products so show the all payment methods
                if ($observer->getEvent()->getMethodInstance()->getCode() == \Kapitus\Finance\Model\Kapitus::METHOD_CODE) {
                    $checkResult = $observer->getEvent()->getResult();
                    $checkResult->setData('is_available', false);
                }
            } else {
                // Cart Item there is no Finance products so show the only Finance payment method and redirect checkout
                if ($this->checkoutSession->getFinMethod()) {
                    if ($observer->getEvent()->getMethodInstance()->getCode() != \Kapitus\Finance\Model\Kapitus::METHOD_CODE) {
                        $checkResult = $observer->getEvent()->getResult();
                        $checkResult->setData('is_available', false);
                    }
                }
            }
        }

    }

    /**
     * @return bool
     */
    public function checkNonFinance()
    {
        foreach ($this->checkoutSession->getQuote()->getAllVisibleItems() as $item) {
            if ($item->getProduct()->getHasFinance()) {
                return false;
            }
        }

        return true;
    }
}