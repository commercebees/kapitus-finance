<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Observer\Frontend;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Helper\Cart;
use Kapitus\Finance\Helper\ProductHelper;
use Kapitus\Finance\Helper\Data;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class CheckoutPreCheck
 *
 * @package Kapitus\Finance\Observer\Frontend
 */
class CheckoutPreCheck implements ObserverInterface
{

    /**
     * @var Session
     */
    protected $checkOutSession;

    /**
     * @var Cart
     */
    protected $cartHelper;

    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var ProductHelper
     */
    protected $productHelperData;

    /**
     * @var ManagerInterface
     */
    protected $managerMessage;

    /**
     * CheckoutPreCheck constructor.
     *
     * @param Session          $checkOutSession
     * @param ProductHelper    $productHelperData
     * @param Data             $helperData
     * @param Cart             $cartHelper
     * @param ManagerInterface $managerMessage
     */
    public function __construct(
        Session $checkOutSession,
        ProductHelper $productHelperData,
        Data $helperData,
        Cart $cartHelper,
        ManagerInterface $managerMessage
    ) {
        $this->managerMessage = $managerMessage;
        $this->cartHelper = $cartHelper;
        $this->helperData = $helperData;
        $this->productHelperData = $productHelperData;
        $this->checkOutSession = $checkOutSession;
    }

    /**
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        return $this->checkOutSession->getQuote();
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if($this->helperData->getScopeConfig('active') == 1){
            if($this->productHelperData->checkCartIsMixed()){
                $this->managerMessage->addErrorMessage('Mixed cart products are not allow to checkout. Only allowed Financal or Non-financial products.');
                $redirectUrl = $this->cartHelper->getCartUrl();
                $observer->getControllerAction()->getResponse()->setRedirect($redirectUrl);
            }
        }
    }
}