<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */
namespace Kapitus\Finance\Observer;

use Kapitus\Finance\Helper\Data;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class ConfigObserver
 *
 * @package Kapitus\Finance\Observer
 */
class ConfigObserver implements ObserverInterface
{

    const CONFIG_API_USER = "payment/kapitus/user_id";

    const CONFIG_API_TOKEN = "payment/kapitus/authtoken";

    const CONFIG_API_ACTIVE = "payment/kapitus/active";

    /**
     * @var ManagerInterface
     */
    protected $messageManger;

    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var EncryptorInterface
     */
    protected $encryptorInterface;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curl;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var null
     */
    protected $isActive =  null;

    /**
     * @var null
     */
    protected $userId =  null;

    /**
     * @var null
     */
    protected $authToken =  null;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $configWriter;

    /**
     * ConfigObserver constructor.
     *
     * @param ManagerInterface                                      $messageManager
     * @param Data                                                  $helperData
     * @param EncryptorInterface                                    $encryptorInterface
     * @param RequestInterface                                      $request
     * @param \Magento\Framework\HTTP\Client\Curl                   $curl
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    public function __construct(
        ManagerInterface $messageManager,
        Data $helperData,
        EncryptorInterface $encryptorInterface,
        RequestInterface $request,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
    ) {
        $this->curl = $curl;
        $this->helperData = $helperData;
        $this->request = $request;
        $this->messageManger = $messageManager;
        $this->encryptorInterface = $encryptorInterface;
        $this->configWriter = $configWriter;
    }

    /**
     * @param $value
     * @return string
     */
    public function getDecryptValue($value)
    {
        return $this->encryptorInterface->decrypt($value);
    }

    /**
     * @param Observer $observer
     *
     * @throws \Zend_Json_Exception
     */
    public function execute(Observer $observer)
    {
        $this->getPostFieldData();

        if($this->isActive == 1 || $this->userId || $this->authToken){
            $params = array( 'user_id' => $this->userId, 'authtoken' => $this->authToken );
            $url  = Data::VERIFY_API_URL.'?user_id='.$this->userId.'&authtoken='.$this->authToken;
            $this->curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
            $this->curl->post($url, $params);
            $response = \Zend_Json::decode($this->curl->getBody(), true);

            if(isset($response['status'])) {
                if($response['status'] == 1){
                    $this->resetEmpty($observer->getWebsite());
                    $this->messageManger->addErrorMessage(_('Kapitus finance payment method API authentication failed. Message: '.$response['message']));
                }
            }
        }
    }

    /**
     * @param int $website
     * @return void
     */
    public function resetEmpty($website = 0)
    {
        $value = "";
        $this->configWriter->save(self::CONFIG_API_ACTIVE, $value, $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $website);
        $this->configWriter->save(self::CONFIG_API_TOKEN, $value, $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $website);
        $this->configWriter->save(self::CONFIG_API_USER, $value, $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $website);
    }

    /**
     * @return void
     */
    public function getPostFieldData()
    {
            $postData = $this->request->getPost();
            if(isset($postData['groups']['kapitus'])){
                $userId = $postData['groups']['kapitus']['fields']['user_id']['value'];
                if($userId != "******"){
                    $this->userId = $userId;
                }

                $authtoken = $postData['groups']['kapitus']['fields']['authtoken']['value'];
                if($authtoken != "******"){
                    $this->authToken = $authtoken;
                }

                if($this->authToken && $this->userId){
                    $this->isActive = $postData['groups']['kapitus']['fields']['active']['value'];
                }
            }
    }
}
