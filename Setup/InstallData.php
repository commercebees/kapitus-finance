<?php
/**
 * Copyright © 2019 Ameex Technologies. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Kapitus\Finance\Setup;

use Magento\Cms\Model\BlockFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class InstallData
 *
 * @package Kapitus\Finance\Setup
 */
class InstallData implements InstallDataInterface
{

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * InstallData constructor.
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param Config          $eavConfig
     * @param BlockFactory    $blockFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory, Config $eavConfig, BlockFactory $blockFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     *
     * @throws \Exception
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $eavSetup = $this->eavSetupFactory->create(array('setup' => $setup));

        $eavSetup->addAttribute(
            Customer::ENTITY,
            'kapitus_customer_id',
            array(
                'type'         => 'varchar',
                'label'        => 'Kapitus Customer Id',
                'input'        => 'text',
                'required'     => false,
                'visible'      => false,
                'user_defined' => false,
                'position'     => 999,
                'input'        => 'text',
                'system'       => 0,
            )
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'has_finance',
            array(
                'type'                    => 'int',
                'backend'                 => '',
                'frontend'                => '',
                'label'                   => 'Eligible for Financing',
                'input'                   => 'boolean',
                'class'                   => '',
                'source'                  => '',
                'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible'                 => true,
                'required'                => false,
            'sort_order'           => 5,
                'user_defined'            => false,
                'default'                 => '',
                'searchable'              => false,
                'filterable'              => false,
                'comparable'              => false,
                'visible_on_front'        => false,
                'used_in_product_listing' => true,
                'unique'                  => false,
                'apply_to'                => '',
            )
        );

        $setup->endSetup();

        $cmsBlockData = array(
            'title'      => 'Kapitus Banner',
            'identifier' => 'kapitus_banner',
            'content'    => "<h1>Add Your Banner Image Here!!</h1>",
            'is_active'  => 1,
            'stores'     => array(0),
            'sort_order' => 0,
        );

        $this->blockFactory->create()->setData($cmsBlockData)->save();
    }
}
