<?php
/**
 * Copyright © 2019 Ameex Technologies. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Kapitus\Finance\Setup;

use Magento\Cms\Model\BlockFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Class UpgradeData
 *
 * @package Kapitus\Finance\Setup
 */
class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * UpgradeData constructor.
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param Config          $eavConfig
     * @param BlockFactory    $blockFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory, Config $eavConfig, BlockFactory $blockFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.0', '<=')) {
            $eavSetup = $this->eavSetupFactory->create(array('setup' => $setup));
            $eavSetup->addAttribute(
                Customer::ENTITY,
                'kapitus_customer_id',
                array(
                    'type'         => 'varchar',
                    'label'        => 'Kapitus Customer Id',
                    'input'        => 'text',
                    'required'     => false,
                    'visible'      => false,
                    'user_defined' => false,
                    'position'     => 999,
                    'input'        => 'text',
                    'system'       => 0,
                )
            );

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'has_finance',
                array(
                    'type'                    => 'int',
                    'backend'                 => '',
                    'frontend'                => '',
                    'label'                   => 'Eligible for Financing',
                    'input'                   => 'boolean',
                    'class'                   => '',
                    'source'                  => '',
                    'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'                 => true,
                    'required'                => false,
                'sort_order'           => 5,
                    'user_defined'            => false,
                    'default'                 => '',
                    'searchable'              => false,
                    'filterable'              => false,
                    'comparable'              => false,
                    'visible_on_front'        => false,
                    'used_in_product_listing' => true,
                    'unique'                  => false,
                    'apply_to'                => '',
                )
            );
        }
    }
}
