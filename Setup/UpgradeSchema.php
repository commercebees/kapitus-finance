<?php
/**
 * Copyright © 2019 Ameex Technologies. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Kapitus\Finance\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema
 *
 * @package Kapitus\Finance\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $orderTable = 'sales_order';
        $quoteTable = 'quote';
        $salesOrderGrid = 'sales_order_grid';

        //Setup column
        if (version_compare($context->getVersion(), '1.0.0', '<=')) {
            //Order tables
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'submission_id',
                    array(
                        'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment'  => 'Submission Id',

                    )
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'finance_status',
                    array(
                        'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment'  => 'Finance Status',

                    )
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'contract_id',
                    array(
                        'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment'  => 'Contract Id',

                    )
                );

            //sales_order_grid table
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($salesOrderGrid),
                    'finance_status',
                    array(
                        'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment'  => 'Finance Status',

                    )
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($salesOrderGrid),
                    'contract_id',
                    array(
                        'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment'  => 'Contract Id',

                    )
                );

            //Quote tables
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'submission_id',
                    array(
                        'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment'  => 'Submission Id',

                    )
                );

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'access_token',
                    array(
                        'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment'  => 'Access Token',

                    )
                );

            $setup->endSetup();
        }
    }
}
