<?php
/**
 * Copyright © 2019 Ameex Technologies. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Kapitus\Finance\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @package Kapitus\Finance\Setup
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();

        $orderTable = 'sales_order';
        $quoteTable = 'quote';
        $salesOrderGrid = 'sales_order_grid';

        //Order table
        $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                'submission_id',
                array(
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment'  => 'Submission Id',

                )
            );
        $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                'finance_status',
                array(
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment'  => 'Finance Status',

                )
            );
        $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                'contract_id',
                array(
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment'  => 'Contract Id',

                )
            );

        //sales_order_grid table
        $setup->getConnection()
            ->addColumn(
                $setup->getTable($salesOrderGrid),
                'finance_status',
                array(
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment'  => 'Finance Status',

                )
            );
        $setup->getConnection()
            ->addColumn(
                $setup->getTable($salesOrderGrid),
                'contract_id',
                array(
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment'  => 'Contract Id',

                )
            );

        //Quote table
        $setup->getConnection()
            ->addColumn(
                $setup->getTable($quoteTable),
                'submission_id',
                array(
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment'  => 'Submission Id',

                )
            );

        $setup->getConnection()
            ->addColumn(
                $setup->getTable($quoteTable),
                'access_token',
                array(
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment'  => 'Access Token',

                )
            );

        //New Order Status
        $data[] = array('status' => 'funding_pending', 'label' => 'In progress');
        $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), array('status', 'label'), $data);
        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status_state'),
            array('status', 'state', 'is_default', 'visible_on_front'),
            array(
                array('funding_pending', 'new', '1', '1'),
            )
        );
        $setup->endSetup();
    }
}
