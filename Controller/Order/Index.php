<?php
/**
 * Copyright © 2019 Ameex Technologies. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Kapitus\Finance\Controller\Order;

class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * Index constructor.
     *
     * @param \Magento\Framework\App\Action\Context             $context
     * @param \Magento\Quote\Model\QuoteFactory                 $quoteFactory
     * @param \Magento\Checkout\Model\Session                   $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface        $storeManager
     * @param \Magento\Catalog\Model\Product                    $product
     * @param \Magento\Quote\Api\CartRepositoryInterface        $cartRepositoryInterface
     * @param \Magento\Quote\Api\CartManagementInterface        $cartManagementInterface
     * @param \Magento\Customer\Model\CustomerFactory           $customerFactory
     * @param \Magento\Quote\Model\QuoteManagement              $quoteManagement
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Sales\Model\Order                        $order
     * @param \Psr\Log\LoggerInterface                          $logger
     * @param \Magento\Framework\Controller\Result\JsonFactory  $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Sales\Model\Order $order,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->quoteFactory = $quoteFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->customerFactory = $customerFactory;
        $this->quoteManagement = $quoteManagement;
        $this->customerRepository = $customerRepository;
        $this->order = $order;
        $this->_logger = $logger;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return $resultJson
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $submissionId = $this->getRequest()->getParam('submission_id');
        if (isset($submissionId)) {
            $quote = $this->quoteFactory->create()->getCollection()->addFieldToFilter('submission_id', $submissionId);
            $quoteData = $quote->getData();
            $quote_id = $quoteData['0']['entity_id'];

            if ($quote_id != '') {
                $order = $this->createMageOrder($quote_id);
                if ($order['status'] == 0) {
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setPath('kapitus/onepage/success');
                    return $resultRedirect;
                } else {
                    return $resultJson->setData(array('success' => false, 'message' => $order['message']));
                }
            }
        } else {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('checkout/cart');
        }

        return $this;
    }

    /**
     * @param $quote_id
     *
     * @return array
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function createMageOrder($quote_id)
    {
        $quote = $this->quoteFactory->create()->load($quote_id);
        $quoteData = $quote->getData();
        $request = $this->getRequest();
        if ($quote->getData('access_token') == $request->getParam('s')) {
            $websiteId = $this->_storeManager->getStore()->getWebsiteId();
            $customer = $this->customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->loadByEmail($quoteData['customer_email']); // load customet by email address
            if ($customer->getEntityId()) {
                // if you have allready buyer id then you can load customer directly
                $customer = $this->customerRepository->getById($customer->getEntityId());
                $quote->assignCustomer($customer); //Assign quote to customer
                $quote->setPaymentMethod('kapitus');
                $quote->getPayment()->importData(array('method' => 'kapitus'));

                $quote->save();

                // Collect Totals
                $quote->collectTotals()->save();

                // Create Order From Quote
                $orderId = $this->cartManagementInterface->placeOrder($quote->getId());
                $order = $this->order->load($orderId);
            } else {
                $quote->setCustomerIsGuest(1);

                $quote->setPaymentMethod('kapitus');
                $quote->getPayment()->importData(array('method' => 'kapitus'));

                $quote->save();

                // Collect Totals
                $quote->collectTotals()->save();

                // Create Order From Quote
                $order = $this->quoteManagement->submit($quote);
            }

            $order->setEmailSent(1);
            $order->setContractId($request->getParam('submission_id'));
            $order->setFinanceStatus('Funding Pending');
            $order->save();
            $this->_checkoutSession->setLastOrderId($order->getId());
            $this->_checkoutSession->setLastRealOrderId($order->getRealOrderId());
            $this->_checkoutSession->setLastSuccessQuoteId($quote->getId());
            $this->_checkoutSession->setLastQuoteId($quote->getId());

            if ($order->getEntityId()) {
                $result = array('status' => 0, 'message' => 'Order Create SuccessFully', 'order_id' => $order->getId());
            } else {
                $result = array('status' => 1, 'message' => 'Something went wrong order cannot be created');
            }

            return $result;
        } else {
            $result = array('status' => 1, 'message' => 'Invalid Access Token');
            return $result;
        }

    }

}

