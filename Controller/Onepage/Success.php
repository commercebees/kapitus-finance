<?php
/**
 * Copyright © 2019 Ameex Technologies. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Kapitus\Finance\Controller\Onepage;

class Success extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\View\Result\PageFactory $pageFactory
) {
        $this->_pageFactory = $pageFactory;
    $this->_checkoutSession = $checkoutSession;
        return parent::__construct($context);
    }

    public function execute()
    {
    if (!$this->isValid()) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
    }

    $this->_checkoutSession->clearQuote();
        return $this->_pageFactory->create();
    }
    public function isValid()
    {
        if (!$this->_checkoutSession->getLastSuccessQuoteId()) {
            return false;
        }

        if (!$this->_checkoutSession->getLastQuoteId() || !$this->_checkoutSession->getLastOrderId()) {
            return false;
        }

        return true;
    }
}
