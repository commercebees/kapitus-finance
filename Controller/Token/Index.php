<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Controller\Token;

/**
 * Class Index
 *
 * @package Kapitus\Finance\Controller\Token
 */
class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $_quoteFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var \Kapitus\Finance\Helper\Data
     */
    protected $_helper;

    /**
     * Index constructor.
     *
     * @param \Magento\Framework\App\Action\Context            $context
     * @param \Magento\Quote\Model\QuoteFactory                $quoteFactory
     * @param \Kapitus\Finance\Helper\Data                     $helper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Kapitus\Finance\Helper\Data $helper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->_quoteFactory = $quoteFactory;
        $this->_helper = $helper;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface |\Magento\Framework\Controller\Result\Json | \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $resultJson = $this->_resultJsonFactory->create();
        $request = $this->getRequest();
        $quote_id = $request->getParam('quote_id');
        if (isset($quote_id)) {
            $quote = $this->_quoteFactory->create()->load($quote_id);
            $email = $this->getRequest()->getParam('email');
            if ($email) {
                $quote->setCustomerEmail($email);
                $quote->getShippingAddress()->setEmail($email);
                $quote->getBillingAddress()->setEmail($email);
                $quote->getBillingAddress()->setRegionId($request->getParam('regionid'));
                $quote->getBillingAddress()->setCity($request->getParam('city'));
                $quote->getBillingAddress()->setCompany($request->getParam('company'));
                $quote->getBillingAddress()->setCountryId($request->getParam('countryid'));
                $quote->getBillingAddress()->setPostcode($request->getParam('postcode'));
                $quote->getBillingAddress()->setStreet($request->getParam('street'));
                $quote->getBillingAddress()->setTelephone($request->getParam('tellephone'));
                $quote->getBillingAddress()->setFirstName($request->getParam('fname'));
                $quote->getBillingAddress()->setLastName($request->getParam('lname'));
                $quote->save();
            }

            if ($quote->getData('submission_id') == '') {
                $getSubmissionId = $this->_helper->getSubmissionId($quote_id);
                if ($getSubmissionId['status'] == 0) {
                    $quote->setSubmissionId($getSubmissionId['id']);
                    $quote->save();
                    if ($quote->getData('access_token') == '') {
                        $quote->setAccessToken($this->_helper->gen_uuid());
                        $quote->save();
                    }
                } else {
                    return $resultJson->setData(
                        array(
                        'success' => false,
                        'error'   => 'Error::' . $getSubmissionId['error_code'] . '::' . $getSubmissionId['message'] . '' . json_encode($getSubmissionId['validation'])
                        )
                    );
                }
            } else {
                $getSubmissionId = array(
                    'status'  => 0,
                    'message' => 'Referal Saved',
                    'id'      => $quote->getData('submission_id')
                );
            }

            $returnUrl = $this->_helper->getBaseUrl() . 'kapitus/Order/?s=' . $quote->getData('access_token');
            $getIframeUrl = $this->_helper->getIframeUrl($getSubmissionId['id'], $returnUrl);
            if ($getIframeUrl['status'] == 0) {
                $getIframeToken = $getIframeUrl['token'];

                $iframeHtml = ' <iframe src="https://apply.kapitus.com/applications/view/?s=' . $getIframeToken . '" width="100%" height="560px" id="kapitus_iframe_finance" sandbox="allow-top-navigation allow-scripts allow-forms">';
                return $resultJson->setData(array('success' => true, 'iframe' => $iframeHtml));
            } else {
                return $resultJson->setData(
                    array(
                    'success' => false,
                    'error'   => 'Error::' . $getIframeUrl['error_code'] . '::' . $getIframeUrl['message']
                    )
                );
            }
        }

        return $resultJson->setData(array('success' => false, 'error' => 'Quote Id not Found'));
    }
}

