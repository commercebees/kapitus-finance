<?php
/**
 * Copyright © 2019 Ameex Technologies. All rights reserved.
 */

namespace Kapitus\Finance\Controller\Token;

/**
 * Class Mixedcart
 *
 * @package Kapitus\Finance\Controller\Token
 */
class Mixedcart extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var \Kapitus\Finance\Helper\ProductHelper
     */
    protected $_kapitusHelper;

    /**
     * Mixedcart constructor.
     *
     * @param \Magento\Framework\App\Action\Context            $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface  $productRepository
     * @param \Kapitus\Finance\Helper\ProductHelper            $kapitusHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Kapitus\Finance\Helper\ProductHelper $kapitusHelper
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_productRepository = $productRepository;
        $this->_kapitusHelper = $kapitusHelper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $resultJson = $this->_resultJsonFactory->create();
        $productId = $this->getRequest()->getPost('product_id');
        $result = array('mixed' => 0);
        if ($productId) {
            $productData = $this->_productRepository->getById($productId);
            $mixed = $this->_kapitusHelper->hasCrossFinance($productData->getHasFinance());
            $result = array('mixed' => $mixed, 'popup' => $this->_kapitusHelper->getCheckoutSession()->getShowpopup());
        }

        return $resultJson->setData($result);
    }
}

