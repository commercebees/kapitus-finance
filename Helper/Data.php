<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Data
 *
 * @package Kapitus\Finance\Helper
 */
class Data extends AbstractHelper
{

    const APPLICATION_URL = 'https://api.kapitus.com/api/application';

    const UPDATE_SUBMISSION_URL = 'https://api.kapitus.com/api/application/';

    const IFRAME_URL = 'https://api.kapitus.com/api/getiframeurl';

    const STATUS_URL = 'https://api.kapitus.com/api/status/';

    const VERIFY_API_URL = "https://api.kapitus.com/api/verify";

    const EMAIL_ORDER_GUEST = "payment_kapitus_guest_template_order";

    const EMAIL_ORDER_NEW = "payment_kapitus_template_order";

    const EMAIL_INVOICE_NEW = "payment_kapitus_template_invoice";

    const EMAIL_INVOICE_GUEST = "payment_kapitus_guest_template_invoice";

    const EMAIL_SHIPMENT_NEW = "payment_kapitus_template_ship";

    const EMAIL_SHIPMENT_GUEST = "payment_kapitus_guest_template_ship";

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $_encryptor;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curlClient;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $regionFactory;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Encryption\EncryptorInterface   $encryptor
     * @param \Magento\Framework\HTTP\Client\Curl                $curl
     * @param \Magento\Quote\Model\QuoteFactory                  $quoteFactory
     * @param \Magento\Directory\Model\RegionFactory             $regionFactory
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->_encryptor = $encryptor;
        $this->curlClient = $curl;
        $this->quoteFactory = $quoteFactory;
        $this->regionFactory = $regionFactory;
        $this->_storeManager = $storeManager;
    }

    /**
     * @param $config_path
     *
     * @return mixed
     */
    public function getScopeConfig($config_path)
    {
        return $this->scopeConfig->getValue(
            'payment/kapitus/' . $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $quote
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getRequestData($quote)
    {
        $quoteData = $quote->getData();
        $productName = array();
        foreach ($quote->getAllVisibleItems() as $item) {
            $productName[] = $item->getProduct()->getName();
        }

        $billingAddressData = $quote->getBillingAddress()->getData();
        $region = $this->regionFactory->create()->load($billingAddressData['region_id']);
        $regionCode = $region->getCode();

        $kapitus_customer_id = $this->gen_uuid();
        $application_request = array(
            "app_type"             => 'R',
            "customer_id"          => $quote->getId(),
            "order_id"             => $quote->getId(),
            "requestedfunding"     => $quoteData['base_grand_total'],
            "merchantdba"          => $billingAddressData['company'],
            "merchantlegalname"    => $this->_storeManager->getStore()->getName(),
            "businessaddress1"     => $billingAddressData['street'],
            "businesscity"         => $billingAddressData['city'],
            "businessstate"        => $regionCode,
            "businesszip"          => $billingAddressData['postcode'],
            "o1name"               => $billingAddressData['firstname'],
            "o1address1"           => $billingAddressData['street'],
            "o1city"               => $billingAddressData['city'],
            "o1state"              => $regionCode,
            "o1zip"                => $billingAddressData['postcode'],
            "email"                => $billingAddressData['email'],
            "locationcontactphone" => $billingAddressData['telephone'],
            "useoffunds"           => json_encode($productName),
            "o1firstname"          => $billingAddressData['firstname'],
            "o1lastname"           => $billingAddressData['lastname'],
            "timing"               => 'Now',

        );
        $this->logger("Request::");
        $this->logger($application_request);
        return $application_request;
    }

    /**
     * @param $quote_id
     *
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSubmissionId($quote_id)
    {
        $quote = $this->quoteFactory->create()->load($quote_id);
        $this->logger("Create Submission");

        $requestData = $this->getRequestData($quote);

        $username = $this->_encryptor->decrypt($this->getScopeConfig('user_id'));
        $authtoken = $this->_encryptor->decrypt($this->getScopeConfig('authtoken'));

        $apiurl = self::APPLICATION_URL . '?user_id=' . $username . '&authtoken=' . $authtoken;

        $this->curlClient->setCredentials($username, $authtoken);
        $this->curlClient->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $this->curlClient->post($apiurl, $requestData);

        $response = $this->curlClient->getBody();
        $responseDecode = json_decode($response, true);
        $this->logger("Response::");
        $this->logger($responseDecode);
        return $responseDecode;
    }

    /**
     * @param $quote_id
     * @param $submissionId
     *
     * @return bool|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateSubmission($quote_id, $submissionId)
    {
        $quote = $this->quoteFactory->create()->load($quote_id);
        $quoteData = $quote->getData();
        $productName = array();
        foreach ($quote->getAllVisibleItems() as $item) {
            $productName[] = $item->getProduct()->getName();
        }

        $shippingAddressData = $quote->getShippingAddress()->getData();
        $region = $this->regionFactory->create()->load($shippingAddressData['region_id']);
        $regionCode = $region->getCode();
        $this->logger("Update Submission");
        $application_request = array(
            "app_type"             => 'R',
            "customer_id"          => $quote->getId(),
            "order_id"             => $quote->getId(),
            "requestedfunding"     => $quoteData['base_grand_total'],
            "merchantdba"          => $shippingAddressData['company'],
            "merchantlegalname"    => $this->_storeManager->getStore()->getName(),
            "businessaddress1"     => $shippingAddressData['street'],
            "businesscity"         => $shippingAddressData['city'],
            "businessstate"        => $regionCode,
            "businesszip"          => $shippingAddressData['postcode'],
            "o1name"               => $shippingAddressData['firstname'],
            "o1address1"           => $shippingAddressData['street'],
            "o1city"               => $shippingAddressData['city'],
            "o1state"              => $regionCode,
            "o1zip"                => $shippingAddressData['postcode'],
            "email"                => $shippingAddressData['email'],
            "locationcontactphone" => $shippingAddressData['telephone'],
            "useoffunds"           => json_encode($productName),
            "o1firstname"          => $shippingAddressData['firstname'],
            "o1lastname"           => $shippingAddressData['lastname'],
            "timing"               => 'Now',

        );
        $this->logger("Request::");
        $this->logger($application_request);
        $requestData = $application_request;//$this->getRequestData($quote);

        $username = $this->_encryptor->decrypt($this->getScopeConfig('user_id'));
        $authtoken = $this->_encryptor->decrypt($this->getScopeConfig('authtoken'));

        $apiurl = self::UPDATE_SUBMISSION_URL . $submissionId . '?user_id=' . $username . '&authtoken=' . $authtoken;

        try {
            $this->curlClient->setCredentials($username, $authtoken);
            $this->curlClient->setOption(CURLOPT_SSL_VERIFYPEER, false);
            $this->curlClient->post($apiurl, $requestData);

            $response = $this->curlClient->getBody();
            $responseDecode = \Zend_Json::decode($response, true);
            $this->logger("Response::");
            $this->logger($responseDecode);
            return $responseDecode;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $submissionId
     * @param $returnUrl
     *
     * @return mixed
     */
    public function getIframeUrl($submissionId, $returnUrl)
    {
        $username = $this->_encryptor->decrypt($this->getScopeConfig('user_id'));
        $authtoken = $this->_encryptor->decrypt($this->getScopeConfig('authtoken'));
        $apiurl = self::IFRAME_URL . '?user_id=' . $username . '&authtoken=' . $authtoken;
        $this->logger("Get Iframe Url");
        $application_request = array(
            "submission_id" => $submissionId,
            "returnurl"     => $returnUrl,
        );
        $this->logger($application_request);
        $this->curlClient->setCredentials($username, $authtoken);
        $this->curlClient->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $this->curlClient->post($apiurl, $application_request);

        $response = $this->curlClient->getBody();
        $responseDecode = json_decode($response, true);
        $this->logger("Response::");
        $this->logger($responseDecode);
        return $responseDecode;
    }

    /**
     * @return string
     */
    public function gen_uuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getDisallowMessage()
    {
        return __('Please complete the purchases you are financing before adding other (non financed) items to your cart.');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getNonDisallowMessage()
    {
        return __('Please complete the purchases you are Non financing before adding other (financed) items to your cart.');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getMixedCartMessage()
    {
        return __('I agree to remove non-financing product from the cart.');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getBuyNowMessage()
    {
        return __("<p>Our financing is backed by <a href='https://kapitus.com/en/the-kapitus-difference/' target='_blank' title='Kapitus'>Kapitus</a>, an industry-leading direct lender who has been providing financing options to businesses since 2006, offering:<p>- Receivables Financing and Equipment Finance products</p><p>- Financing Amounts from $5,000 to $1 million</p>- Terms from 3 to 72 months</p><p>Interested in longer term financing?  Visit us <a href='https://apply.kapitus.com/'>here</a> to see what other products and services we can offer.</p>Amounts are based on approval from Kapitus. To apply for financing select the continue button below:</p><p>How to apply:</p><p>- Complete the short online application</p><p>- You will receive a response within one business day</p>- No payment is required at the time of purchasing. When qualified, your order will be processed and a tracking number emailed to you.</p>");
    }

    /**
     * @param $content
     */
    public function logger($content)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/kapitus.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($content);
    }

    /**
     * @param $path
     *
     * @return bool|mixed
     */
    public function getStoreConfig($path)
    {
        if ($path) {
            return $this->scopeConfig->getValue(
                $path,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        }

        return false;
    }
}
