<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Checkout\Model\Session as QuoteSession;
use Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Store\Model\StoreManagerInterface;

/**
 * Class ProductHelper
 *
 * @package Kapitus\Finance\Helper
 */
class ProductHelper extends AbstractHelper
{

    /**
     * @var QuoteSession
     */
    protected $quoteSession;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * ProductHelper constructor.
     *
     * @param Context               $context
     * @param QuoteSession          $quoteSession
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        QuoteSession $quoteSession,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->quoteSession = $quoteSession;
    }

    /**
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        return $this->quoteSession->getQuote();
    }

    /**
     * @return \Magento\Checkout\Model\Session
     */
    public function getCheckoutSession()
    {
        return $this->quoteSession;
    }

    /**
     * @param $hasFinance
     *
     * @return bool
     */
    public function hasCrossFinance($hasFinance)
    {
        foreach ($this->getQuote()->getAllVisibleItems() as $item) {
            if ($item->getProduct()->getHasFinance() != $hasFinance) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isFinanceCart()
    {

        foreach ($this->getQuote()->getAllVisibleItems() as $item) {
            if ($item->getProduct()->getHasFinance()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function checkCartIsMixed()
    {
        $fin = false;
        $nonFin = false;
        foreach ($this->getQuote()->getAllVisibleItems() as $item) {
            if ($item->getProduct()->getHasFinance()) {
                $fin = true;
            } else {
                $nonFin = true;
            }
        }

        if ($fin && $nonFin) {
            return true;
        }

        return false;
    }

    /**
     * @return \Magento\Store\Api\Data\StoreInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStore()
    {
        return $this->storeManager->getStore();
    }
}
