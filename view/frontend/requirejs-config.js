/*
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */
var config = {
    map: {
        '*': {
            'Magento_Tax/template/checkout/summary/grand-total.html': 'Kapitus_Finance/template/cart/totals/grand-total.html',
            'Magento_Tax/js/view/checkout/summary/grand-total': 'Kapitus_Finance/js/view/checkout/summary/grand-total'
        },
    },
    config: {
        mixins: {
            'Magento_Checkout/js/model/checkout-data-resolver': {
                'Kapitus_Finance/js/model/checkout-data-resolver': true
            },
            "Magento_Checkout/js/sidebar": {
                'Kapitus_Finance/js/sidebar-mixin': true
            },
            'Magento_Catalog/js/catalog-add-to-cart': {
                'Kapitus_Finance/js/catalog-add-to-cart-mixin': true
            }
        }
    }
};




