/*
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

require(
    [
        'jquery',
        'domReady!'
    ], function ($) {
        'use strict';
        $(".product-item-info .product-item-details .kapitus_label_div").each(function () {
            var now = $(this);
            $(now).insertBefore($(this).parents(".product-item-details").siblings("a"));
            $('.catalog-category-view .kapitus_label_div').show();
        });

    });
