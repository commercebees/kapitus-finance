/*
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */
define([
    'jquery',
    'Magento_Customer/js/customer-data',
], function ($,customerData) {
    return function(originalWidget) {

        $.widget('mage.sidebar', $.mage.sidebar, {
            _removeItemAfter: function (elem) {

                var productData = this._getProductById(Number(elem.data('cart-item')));

                if (!_.isUndefined(productData)) {
                    $(document).trigger('ajax:removeFromCart', {
                        productIds: [productData['product_id']]
                    });
                }

                if($("body").hasClass("catalog-product-view")){
                    window.location.reload();
                }
            },
        });

        return $.mage.sidebar;
    }
});
