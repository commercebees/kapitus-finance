define([
    'jquery',
    'mage/translate',
    'jquery/ui',
], function ($, $t,alert) {
    'use strict';

    return function (widget) {
    console.log('catalog-add-to-cart-mixin');

    $.widget('mage.catalogAddToCart', widget, {
        
        _redirect: function (url) {

            var urlParts = url.split('#'),
                locationParts = window.location.href.split('#'),
                forceReload = urlParts[0] === locationParts[0];

            if($('#finance').val() && urlParts.length > 1){
                url = urlParts[0];
            }

            window.location = url;

            if (forceReload) {
                window.location.reload();
            }
        }
    });

    return $.mage.catalogAddToCart;
  }
});