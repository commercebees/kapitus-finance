/*
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push({
            type: 'kapitus',
            component: 'Kapitus_Finance/js/view/payment/method-renderer/kapitus-method'
        });
        /** Add view logic here if needed */
        return Component.extend({});
    }
);