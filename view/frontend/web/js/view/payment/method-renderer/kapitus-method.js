/*
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Ui/js/modal/modal',
        'jquery/ui'
    ],
    function ($, Component, additionalValidators, quote, fullScreenLoader, modal) {
        'use strict';
        var baseurl = window.checkoutConfig.payment.kapitus.base_url;
        var quoteId = window.checkoutConfig.payment.kapitus.quote_id;
        var options = {
            type: 'popup',
            responsive: true,
            innerScroll: false,
            buttons: [],
            clickableOverlay: false,
            focus: '',
            modalClass: 'kapitus-fin-popup'
        };
        return Component.extend({
            defaults: {
                template: 'Kapitus_Finance/payment/kapitus'
            },
showIframe: function() {
    $("#kapitus_iframe").html("<center> Please wait. We are processing your request.... </center>");
    var popup = modal(options, $('#kapitus_iframe'));
    $('.kapitus-fin-popup .action-close').hide();
    var checkbox = document.getElementById('is_financed');
           if (this.validate() && additionalValidators.validate()) {
            if (quote.billingAddress() !== null && quote.shippingAddress() !== null) {
                var billingCompany = quote.billingAddress().company; //$.trim($("input[name='company']").val());
                var shippingCompany = quote.shippingAddress().company;
                if (billingCompany == '' || billingCompany == undefined) {
                    alert('Please fill the company field in billing address form');
                } else if (shippingCompany == '' || shippingCompany == undefined) {
                    alert('Please fill the company field in shipping address form');
                } else {

                    $("#kapitus_iframe").modal("openModal");
                    $('.loading-fin').show();

                    //$("#kapitus").prop("checked", true);
                    if (!document.getElementById("kapitus_iframe_finance")) {
                        if (this.getIframe() != '') {
                            var div = document.getElementById('kapitus_iframe');
                            div.innerHTML += this.getIframe();
                            return true;
                        }
                    } else {
                        $('#kapitus_iframe').show();
                        $('#kapitus_iframe_finance').show();
                        return true;
                    }
                }
            } else {
                alert("Shipping/Billing address may not be filled. Please verify and fill the address.");
                return false;
            }
        } else {
            alert("Please fill all the required fields");
            return false;
        }
},
            activatePlaceorder:function(){
                if( $('#is_financed').is(':checked'))
                {
                    $('.agree-placeorder').removeAttr('disabled');  
                    return true;                  
                }
                else{
                     $('.agree-placeorder').attr('disabled','disabled');
                      $('#is_financed').prop("checked",false);
                      return true;
                }
            },
            getIframe: function () {
                var iframe = '';
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'kapitus/token',
                    showLoader: true,
                    timeout: 30000,
                    data: {
                        quote_id: quoteId,
                        email: this.getEmail(),
                        fname: quote.billingAddress().firstname,
                        lname: quote.billingAddress().lastname,
                        regionid: quote.billingAddress().regionId,
                        city: quote.billingAddress().city,
            company: quote.billingAddress().company,
                        countryid: quote.billingAddress().countryId,
                        postcode: quote.billingAddress().postcode,
                        street: quote.billingAddress().street[0],
                        tellephone: quote.billingAddress().telephone
                    },
                    success: function (response) {
                        $('.loading-fin').hide();
                        if (response.success == false) {
                            $("#kapitus_iframe").html("<p>" + response.error + "</p>");
                $(".kapitus-fin-popup .modal-inner-wrap header").show();
                            $('.kapitus-fin-popup .action-close').show();
                            console.log(response);
                        } else if (response.success == true) {
                            $("#kapitus_iframe").html(response.iframe);
                $('#is_financed').prop('checked', true);
                            iframe = response.iframe;
                            console.log(response);
                        } else {
                            $("#kapitus_iframe").html("<p> Network request Fail </p>");
                $(".kapitus-fin-popup .modal-inner-wrap header").show();
                            $('.kapitus-fin-popup .action-close').show();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                        if (jqXHR.status == 500) {
                            var errorData = 'Internal error: ' + jqXHR.responseText;
                        } else{
                            var errorData = 'Unexpected error :'+textStatus;
                        }
                        $("#kapitus_iframe").html("<p>" + errorData + "</p>");
            $(".kapitus-fin-popup .modal-inner-wrap header").show();
                        $('.kapitus-fin-popup .action-close').show();
                    }

                });
                return iframe;
            },
            /** Returns customer Email */
            getEmail: function () {
                if (quote.guestEmail) return quote.guestEmail;
                else return window.checkoutConfig.customerData.email;
            },

        });
    }
);
