define(
    [
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/action/select-payment-method'
    ], function(
        paymentService,
        checkoutData,
        selectPaymentMethodAction
    ) {
        'use strict';
	var has_finance = window.checkoutConfig.payment.kapitus.has_finance;

        return function(checkoutDataResolver) {
            checkoutDataResolver.resolvePaymentMethod = function() {
                var availablePaymentMethods = paymentService.getAvailablePaymentMethods(),
                    selectedPaymentMethod = checkoutData.getSelectedPaymentMethod();
		if (has_finance == 1){
			var paymentMethod = selectedPaymentMethod ? selectedPaymentMethod : 'kapitus';		
		}else{
			var paymentMethod = selectedPaymentMethod;
		}

                if (availablePaymentMethods.length > 0) {
                    availablePaymentMethods.some(function (payment) {
                        if (payment.method == paymentMethod) {
                            selectPaymentMethodAction(payment);
                        }
                    });
                }
            };

            return checkoutDataResolver;
        };
    });
