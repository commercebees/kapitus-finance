<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kapitus_Finance',
    __DIR__
);

