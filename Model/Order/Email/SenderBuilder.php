<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Model\Order\Email;

use Magento\Framework\DataObject;

/**
 * Class SenderBuilder
 *
 * @package Kapitus\Finance\Model\Order\Email
 */
class SenderBuilder extends \Magento\Sales\Model\Order\Email\SenderBuilder
{

    /**
     * @return void
     */
    protected function configureEmailTemplate()
    {
        parent::configureEmailTemplate();

        $templateVar = $this->templateContainer->getTemplateVars();
        $transportObject = new DataObject($templateVar);
        $templateId = "";
        if (is_a($this->identityContainer, 'Magento\Sales\Model\Order\Email\Container\OrderIdentity')) {
            $templateId = ($transportObject->getOrder()->getCustomerIsGuest()) ? \Kapitus\Finance\Helper\Data::EMAIL_ORDER_GUEST : \Kapitus\Finance\Helper\Data::EMAIL_ORDER_NEW;
        }

        if (is_a($this->identityContainer, 'Magento\Sales\Model\Order\Email\Container\ShipmentIdentity')) {
            $templateId = ($transportObject->getOrder()->getCustomerIsGuest()) ? \Kapitus\Finance\Helper\Data::EMAIL_SHIPMENT_GUEST : \Kapitus\Finance\Helper\Data::EMAIL_SHIPMENT_NEW;
        }

        if (is_a($this->identityContainer, 'Magento\Sales\Model\Order\Email\Container\InvoiceIdentity')) {
            $templateId = ($transportObject->getOrder()->getCustomerIsGuest()) ? \Kapitus\Finance\Helper\Data::EMAIL_INVOICE_GUEST : \Kapitus\Finance\Helper\Data::EMAIL_INVOICE_NEW;
        }

        if ($templateId) {
            $isPayment = ($transportObject->getOrder()->getPayment()->getMethod() == \Kapitus\Finance\Model\Kapitus::METHOD_CODE) ? true : false;
            if ($isPayment) {
                $this->transportBuilder->setTemplateIdentifier($templateId);
            }else{
                $templateIdWithoutFin = $templateId.'_without';
                $this->transportBuilder->setTemplateIdentifier($templateIdWithoutFin);
            }
        }
    }
}
