<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */
namespace Kapitus\Finance\Model\Product\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 *
 * @package Kapitus\Finance\Model\Product\Attribute\Source
 */
class Status extends AbstractSource implements SourceInterface, OptionSourceInterface
{

    const STATUS_ENABLED = 1;

    const STATUS_DISABLED = 0;

    /**
     * @return array
     */
    public function getVisibleStatusIds()
    {
        return array(self::STATUS_ENABLED);
    }

    /**
     * @return array
     */
    public function getSaleableStatusIds()
    {
        return array(self::STATUS_ENABLED);
    }

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return array(self::STATUS_ENABLED => __('Yes'), self::STATUS_DISABLED => __('No'));
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $result = array();

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = array('value' => $index, 'label' => $value);
        }

        return $result;
    }

    /**
     * @param int|string $optionId
     *
     * @return bool|mixed|string|null
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }

    /**
     * @param \Magento\Eav\Model\Entity\Collection\AbstractCollection $collection
     * @param string                                                  $dir
     *
     * @return $this|AbstractSource
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addValueSortToCollection($collection, $dir = 'asc')
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $attributeId = $this->getAttribute()->getId();
        $attributeTable = $this->getAttribute()->getBackend()->getTable();

        if ($this->getAttribute()->isScopeGlobal()) {
            $tableName = $attributeCode . '_t';

            $collection->getSelect()->joinLeft(
                array($tableName => $attributeTable),
                "e.entity_id={$tableName}.entity_id" .
                " AND {$tableName}.attribute_id='{$attributeId}'" .
                " AND {$tableName}.store_id='0'",
                array()
            );

            $valueExpr = $tableName . '.value';
        } else {
            $valueTable1 = $attributeCode . '_t1';
            $valueTable2 = $attributeCode . '_t2';

            $collection->getSelect()->joinLeft(
                array($valueTable1 => $attributeTable),
                "e.entity_id={$valueTable1}.entity_id" .
                " AND {$valueTable1}.attribute_id='{$attributeId}'" .
                " AND {$valueTable1}.store_id='0'",
                array()
            )->joinLeft(
                array($valueTable2 => $attributeTable),
                "e.entity_id={$valueTable2}.entity_id" .
                " AND {$valueTable2}.attribute_id='{$attributeId}'" .
                " AND {$valueTable2}.store_id='{$collection->getStoreId()}'",
                array()
            );

            $valueExpr = $collection->getConnection()->getCheckSql(
                $valueTable2 . '.value_id > 0',
                $valueTable2 . '.value',
                $valueTable1 . '.value'
            );
        }

        $collection->getSelect()->order($valueExpr . ' ' . $dir);

        return $this;
    }
}
