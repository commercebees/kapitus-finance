<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Model;

/**
 * Class Kapitus
 *
 * @package Kapitus\Finance\Model
 */
class Kapitus extends \Magento\Payment\Model\Method\AbstractMethod
{

    const METHOD_CODE = "kapitus";
    /**
     * @var string
     */
    protected $_code = self::METHOD_CODE;

    /**
     * @var bool
     */
    protected $_isOffline = true;

}

