<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

/**
 * Class ConfigProvider
 *
 * @package Kapitus\Finance\Model
 */
class ConfigProvider implements ConfigProviderInterface
{

    const CODE = 'kapitus';

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * ConfigProvider constructor.
     *
     * @param \Magento\Checkout\Model\Session            $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {

        $this->checkoutSession = $checkoutSession;
        $this->_storeManager = $storeManager;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getConfig()
    {
        return array(
            'payment' => array(
                self::CODE => array(
                    'base_url' => $this->_storeManager->getStore()->getBaseUrl(),
                    'quote_id' => $this->checkoutSession->getQuote()->getId(),
            'has_finance' => $this->checkFinance(),
                ),
            ),
        );
    }
    public function checkFinance()
    {
    $quoteItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
    foreach($quoteItems as $quoteItem){
        if($quoteItem->getProduct()->getHasFinance() == 1)
        {
            return $quoteItem->getProduct()->getHasFinance();
        }
    }

    return 0;
    }
}
