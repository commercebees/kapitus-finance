<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Model\Admin\VersionUpdate;

use Magento\Backend\Model\UrlInterface;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Notification\MessageInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Messages
 *
 * @package Kapitus\Finance\Model\Admin\VersionUpdate
 */
class Messages implements MessageInterface
{

    /**
     * @var UrlInterface
     */
    protected $backendUrl;
    /**
     * @var Session
     */
    protected $authSession;

    /**
     * @var VersionNotifier
     */
    protected $versionNotifier;

    /**
     * @var ScopeConfigInterface
     */
    protected $configInterface;

    /**
     * Messages constructor.
     *
     * @param UrlInterface         $backendUrl
     * @param Session              $authSession
     * @param ScopeConfigInterface $configWriter
     * @param VersionNotifier      $versionNotifier
     */
    public function __construct(
        UrlInterface $backendUrl,
        Session $authSession,
        ScopeConfigInterface $configWriter,
        VersionNotifier $versionNotifier
    ) {
        $this->authSession = $authSession;
        $this->backendUrl = $backendUrl;
        $this->versionNotifier = $versionNotifier;
        $this->configInterface = $configWriter;
    }

    /**
     * @return \Magento\Framework\Phrase|string
     */
    public function getText()
    {
        $currentversion = $this->versionNotifier->getCurrentPackageVersion();
        $message = __("Kapitus Financing extension release is available, Current Version :" . $currentversion['version'] . " New release:" . $this->configInterface->getValue('kapitus/version/check') . "  <a href='" . $this->versionNotifier->getAdminBaseUrl() . "'>Click here</a> to update the extension");
        return $message;
    }

    /**
     * @return string
     */
    public function getIdentity()
    {
        return md5('Kapitus_Finance' . $this->authSession->getUser()->getLogdate());
    }

    /**
     * @return bool
     */
    public function isDisplayed()
    {
        $currentversion = $this->versionNotifier->getCurrentPackageVersion();
        if($this->checkUpdateAvailableConfig()=='' || $this->checkUpdateAvailableConfig()==NULL){
            return false;
        }
        elseif ($this->checkUpdateAvailableConfig() != $currentversion['version']) {
            return true;
        }
        else{    
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function checkUpdateAvailableConfig()
    {
        return $this->configInterface->getValue('kapitus/version/check');
    }

    /**
     * @return int
     */
    public function getSeverity()
    {
        return \Magento\Framework\Notification\MessageInterface::SEVERITY_CRITICAL;
    }
}