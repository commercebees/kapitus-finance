<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Model\Admin\VersionUpdate;

use Magento\Backend\Model\UrlInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Module\Dir\Reader;

/**
 * Class VersionNotifier
 *
 * @package Kapitus\Finance\Model\Admin\VersionUpdate
 */
class VersionNotifier
{

    const PACKAGE_URL = 'https://packagist.org/packages/';

    const CONFIG_PATH_VERSION = 'kapitus/version/check';

    /**
     * @var UrlInterface
     */
    protected $urlBackend;

    protected $moduleReader;

    /**
     * @var WriterInterface
     */
    protected $configInterface;

    /**
     * VersionNotifier constructor.
     *
     * @param UrlInterface    $urlBackend
     * @param Reader          $moduleReader
     * @param WriterInterface $configWriter
     */
    public function __construct(
        UrlInterface $urlBackend,
        Reader $moduleReader,
        WriterInterface $configWriter
    ) {
        $this->moduleReader = $moduleReader;
        $this->urlBackend = $urlBackend;
        $this->configInterface = $configWriter;
    }

    /**
     * @return string
     */
    public function getDirectory()
    {
        return $this->moduleReader->getModuleDir(
            \Magento\Framework\Module\Dir::MODULE_ETC_DIR,
            'Kapitus_Finance'
        );
    }

    /**
     * @return array
     */
    public function getCurrentPackageVersion()
    {
        $moduleJson = file_get_contents($this->getDirectory() . '/../composer.json');
        $jsonData = json_decode($moduleJson, true);
        return array('version' => $jsonData['version'], 'package_name' => $jsonData['name']);
    }

    /**
     * @return string
     */
    public function getAdminBaseUrl()
    {
        return $this->urlBackend->getUrl('adminhtml/backendapp/redirect/app/setup/');
    }

    /**
     * @return mixed
     */
    public function getReleasePackageVersion()
    {
        $moduleJson = file_get_contents('https://packagist.org/packages/kapitus/kapitus-finance.json');
        $jsonData = json_decode($moduleJson, true);
        $version = $jsonData['package']['versions'];
        $versioncounter = array();
        foreach ($version as $version_number => $versionData) {
            if ($version_number != 'dev-master') {
                $versioncounter[] = $version_number;
            }
        }

        return $versioncounter[0];
    }

    public function UpdateAvailable()
    {
        $decreleaseversion = $this->getReleasePackageVersion();
        $this->configInterface->save(self::CONFIG_PATH_VERSION, $decreleaseversion);
    }
}
