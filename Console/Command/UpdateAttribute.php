<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\Product\Action;
use Kapitus\Finance\Helper\Data;
use Magento\Framework\Encryption\EncryptorInterface;

/**
 * Class UpdateAttribute
 *
 * @package Kapitus\Finance\Console\Command
 */
class UpdateAttribute extends Command
{

    const NAME_ARGUMENT = "name";

    const NAME_OPTION = "value";
    
    /**
     * @var
     */
    protected $action;
    
    /**
     * @var
     */
    protected $logger;

    /**
     * @var
     */
    protected $productCollectionFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * FinanceStatusUpdate constructor.
     *
     * @param Action                         $action
     * @param LoggerInterface              $logger
     * @param CollectionFactory            $productCollectionFactory
     * @param Data                         $helper
     * @param EncryptorInterface           $encryptor
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        Action $action,
        LoggerInterface $logger,
        CollectionFactory $productCollectionFactory,
        Data $helper,
        EncryptorInterface $encryptor,
        \Magento\Framework\App\State $state
    ) {
        $this->state = $state;
        $this->action = $action;
        $this->logger = $logger;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->helper = $helper;
        $this->encryptor = $encryptor;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);

        if ($option == 0 ) {
            $productIds = $this->getProductIdCollection();
            $this->writerLogger("Start");        
            $this->action->updateAttributes($productIds, array('has_finance' => $option), 0);
            $this->writerLogger("End");
        } elseif($option == 1) {
            $productIds = $this->getProductIdCollection();
            $this->writerLogger("Start");        
            $this->action->updateAttributes($productIds, array('has_finance' => $option), 0);
            $this->writerLogger("End");
        } else {
            $output->writeln("Option Value only 0 or 1");    
        }
        
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kapitus:update-attribute");
        $this->setDescription("Update Finance with kapitus attribute");
        $this->setDefinition(
            array(
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_REQUIRED, "Attribute Value")
            )
        );
        parent::configure();
    }

    /**
     * @return array
     */
    public function getProductIdCollection()
    {

        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        
        $array_product = array();
        foreach ($collection as $product) {
            $array_product[] = $product->getId();
        }

        return $array_product;
    }

    public function writerLogger($content)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/update-attribute.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($content);
    }
    
}
