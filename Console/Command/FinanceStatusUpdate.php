<?php
/**
 * Copyright (c) 2019. Ameex Technologies . All rights reserved.
 */

namespace Kapitus\Finance\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\HTTP\Client\Curl;
use Kapitus\Finance\Helper\Data;
use Magento\Framework\Encryption\EncryptorInterface;

/**
 * Class FinanceStatusUpdate
 *
 * @package Kapitus\Finance\Console\Command
 */
class FinanceStatusUpdate extends Command
{

    const NAME_ARGUMENT = "name";

    const NAME_OPTION = "option";

    const FIN_COMPLETE_STATUS = "funded";

    const STATUS_API_ENDPOINT = "https://api.kapitus.com/api/status/";

    /**
     * @var
     */
    protected $curl;

    /**
     * @var
     */
    protected $logger;

    /**
     * @var
     */
    protected $orderFactory;

    /**
     * @var
     */
    protected $ordercollectionFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * FinanceStatusUpdate constructor.
     *
     * @param Curl                         $curl
     * @param LoggerInterface              $logger
     * @param OrderFactory                 $orderFactory
     * @param CollectionFactory            $ordercollectionFactory
     * @param Data                         $helper
     * @param EncryptorInterface           $encryptor
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        Curl $curl,
        LoggerInterface $logger,
        OrderFactory $orderFactory,
        CollectionFactory $ordercollectionFactory,
        Data $helper,
        EncryptorInterface $encryptor,
        \Magento\Framework\App\State $state
    ) {
        $this->state = $state;
        $this->curl = $curl;
        $this->logger = $logger;
        $this->orderFactory = $orderFactory;
        $this->ordercollectionFactory = $ordercollectionFactory;
        $this->helper = $helper;
        $this->encryptor = $encryptor;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);

        $orderData = $this->getOrderCollection();
        if (!$orderData['status']) {
            $output->writeln($orderData['message'] . $name);
            return;
        }

        $username = $this->encryptor->decrypt($this->helper->getScopeConfig('user_id'));
        $authtoken = $this->encryptor->decrypt($this->helper->getScopeConfig('authtoken'));

        foreach ($orderData['order'] as $order) {
            $params = array('user_id' => $username, 'authtoken' => $authtoken);
            $url = self::STATUS_API_ENDPOINT . $order->getSubmissionId() . "?user_id=" . $username . "&authtoken=" . $authtoken;

            $this->curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
            $this->curl->post($url, $params);
            $response = \Zend_Json::decode($this->curl->getBody(), true);

        $this->writerLogger($url);
        $this->writerLogger($response);

        $responseObject = new \Magento\Framework\DataObject($response);
    
            if ($responseObject->hasCurrentstatus()) {
                $orderObj = $this->orderFactory->create()->load($order->getId());

                $currentStatus = trim($responseObject->getCurrentstatus());
                if ($orderObj->getFinanceStatus() != $currentStatus) {
                    $orderObj->setFinanceStatus($currentStatus);

            if($responseObject->hasContractId()){
            $orderObj->setContractId($responseObject->getContractId());
            }

                    $message = "Order Finance Status updated: " . $currentStatus . " and order status note : " . $responseObject->getCurrentstatusnote();

                    $orderObj->addStatusToHistory($orderObj->getStatus(), $message, true);
                    $orderObj->save();
                }
            }
        } // For each end
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kapitus:finance-update");
        $this->setDescription("Order finance status will updated from kapitus API. Please schedule the cmd into cronjob every 5 minutes ");
        $this->setDefinition(
            array(
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
            )
        );
        parent::configure();
    }

    /**
     * @return array
     */
    public function getOrderCollection()
    {

        $collection = $this->ordercollectionFactory->create();
        $collection->addFieldToSelect('*')
            ->addFieldToFilter('finance_status', array('neq' => self::FIN_COMPLETE_STATUS))
            ->addFieldToFilter('submission_id', array('neq' => ""));

        /* join with payment table */
        $collection->getSelect()
            ->join(
                array("sop" => $collection->getTable("sales_order_payment")),
                'main_table.entity_id = sop.parent_id',
                array('method')
            )
            ->where('sop.method = ?', \Kapitus\Finance\Model\Kapitus::METHOD_CODE);

        if ($collection->getSize() > 0) {
            return array('status' => true, 'order' => $collection);
        } else {
            return array('status' => false, 'message' => "There no order to update from Kapitus API");
        }
    }

    public function writerLogger($content)
    {
    $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/status-update-kapitus.log');
    $logger = new \Zend\Log\Logger();
    $logger->addWriter($writer);
    $logger->info($content);
    }
    
}
